# Utilisez l'image de base appropriée pour votre application
FROM molgenis/maven-jdk15:latest

# Répertoire de travail
WORKDIR /app

# Copie des fichiers de l'application dans le conteneur
COPY . .

# Installation des dépendances et compilation
RUN mvn package

CMD java -jar target/svod-0.0.1-SNAPSHOT.jar
